﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheDuckLib.Base;
using TheDuckLib.Interfaces;

namespace TheDuck.Implementation
{
    public class Mallard : Duck, ISwimable, IFlyable, ISquawkable
    {
        public override void display()
        {
            Console.WriteLine("Soy un pato real!!");
        }

        public override void doAll()
        {
            fly();
            squawk();
            swim();
        }

        public void fly()
        {
            Console.WriteLine("Estoy volando!!");
        }

        public void squawk()
        {
            Console.WriteLine("Estoy graznando!!");
        }

        public void swim()
        {
            Console.WriteLine("Estoy nadando!!");
        }
    }
}
