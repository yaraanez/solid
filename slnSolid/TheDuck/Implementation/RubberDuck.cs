﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheDuckLib.Base;
using TheDuckLib.Interfaces;

namespace TheDuck.Implementation
{
    public class RubberDuck : Duck, ISwimable, ISquawkable
    {
        public override void doAll()
        {
            swim();
            squawk();            
        }

        public override void display()
        {
            Console.WriteLine("Soy un pato de Goma");
        }

        public void swim()
        {
            Console.WriteLine("Soy un pato de Goma nadando");
        }

        public void squawk()
        {
            Console.WriteLine("Soy un pato de Goma sonando");
        }
    }
}
