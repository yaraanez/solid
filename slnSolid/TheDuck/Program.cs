﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheDuck.Implementation;
using TheDuckLib.Base;
using TheDuckLib.Behaviors;
using TheDuckLib.Interfaces;

namespace TheDuck
{
    class Program
    {
        static void Main(string[] args)
        {
            DuckCollection<Duck> Ducks = new DuckCollection<Duck>();

            Mallard Mallard = new Mallard();
            RubberDuck RubberDuck = new RubberDuck();

            Ducks.Add(Mallard);
            Ducks.Add(RubberDuck);
            Ducks.printDucks();

            Console.ReadLine();
        }
    }
}
