﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheDuckLib.Interfaces;

namespace TheDuckLib.Base
{
    public abstract class Duck : IDuck
    {
        public string Name { get; set; }
        public virtual void display()
        {
            Console.WriteLine("Soy un pato.");
        }

        public abstract void doAll();
    }
}
