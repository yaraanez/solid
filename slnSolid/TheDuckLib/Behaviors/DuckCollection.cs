﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheDuckLib.Base;
using TheDuckLib.Interfaces;

namespace TheDuckLib.Behaviors
{
    public class DuckCollection<T>: List<T>, IPrintableDucks where T: Duck
    {
        public void printDucks()
        {
            foreach (T Duck in this)
            {
                Console.WriteLine();
                Duck.display();
                Duck.doAll();
            }
        }
    }
}
