﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheDuckLib.Interfaces
{
    //Por regla de negocio, un pato es mostrable
    public interface IDuck : IDisplayable
    {
        void doAll();
    }
}
