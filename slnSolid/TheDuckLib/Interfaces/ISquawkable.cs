﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheDuckLib.Interfaces
{
    public interface ISquawkable
    {
        void squawk();
    }
}
